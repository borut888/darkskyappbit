//
//  RootViewController.swift
//  DarkSkyApp
//
//  Created by Borut on 15/04/2018.
//  Copyright © 2018 Borut. All rights reserved.
//

import UIKit
import Moya

class RootViewController: UIViewController {
    let providerWeather = MoyaProvider<WeatherApi>()
    var weatherObjects: WeatherModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func getWeatherDataUserLocation(lat: Float, long: Float){
        providerWeather.request(.cityLatAndLon(lat: lat, long: long)) { result in
            switch result {
            case let .success(response):
                do {
                    let decoder = JSONDecoder()
                    let weatherDownload = try decoder.decode(WeatherModel.self, from: response.data)
                    self.weatherObjects = weatherDownload
//                    DispatchQueue.main.async {
//                        //self.locationManager.stopUpdatingLocation()
//                        self.activityIndicator.stopAnimating()
//                        self.updateUI()
//                    }
                }catch let err  {
                    print(" there is some error: \(err)" )
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
